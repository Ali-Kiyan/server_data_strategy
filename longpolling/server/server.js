const express = require('express');
const app = express();
const port = 3000;

let listeners = [];

// Function to notify all listeners
const notifyListeners = (data) => {
    listeners.forEach((res) => {
        res.json(data);
    });
    listeners = [];
};

// Simulate data generation
setInterval(() => {
    const newData = { message: 'New data generated!' };
    notifyListeners(newData);
}, 10000); // Generate data every 10 seconds

// Endpoint for long polling
app.get('/get-updates', (req, res) => {
    listeners.push(res);
    
    // Set timeout to handle long polling timeout
    req.setTimeout(30000, () => {
        res.json({ message: 'No new updates' });
        listeners = listeners.filter(listener => listener !== res);
    });
});

app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`);
});
