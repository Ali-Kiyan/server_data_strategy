const express = require('express');
const http = require('http');
const path = require('path');
const socketIo = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

let interval;
const paragraph = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lacinia odio vitae vestibulum vestibulum. Cras venenatis euismod malesuada.";

// Serve the client files
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../client/index.html'));
});

io.on('connection', (socket) => {
  console.log('a user connected');

  // Handle start streaming request from client
  socket.on('start-stream', () => {
    console.log('start streaming');
    let index = 0;
    interval = setInterval(() => {
      if (index < paragraph.length) {
        socket.emit('text-stream', paragraph.charAt(index));
        index++;
      } else {
        clearInterval(interval);
      }
    }, 10); // Stream character every 100ms
  });

  // Handle stop streaming request from client
  socket.on('stop-stream', () => {
    console.log('stop streaming');
    clearInterval(interval);
  });

  socket.on('disconnect', () => {
    console.log('user disconnected');
    clearInterval(interval);
  });
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
