const express = require('express');
const http = require('http');
const socketIo = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketIo(server, {
    cors: {
        origin: "http://localhost:3000", // Replace with your client app's URL
        methods: ["GET", "POST"]
    }
});

const messages = [
    "This is first message",
    "This is second message",
    "This is third message",
    "This is forth message"
];

io.on('connection', socket => {
    console.log('Client connected');
    let messageIndex = 0;
    let wordIndex = 0;

    const sendData = () => {
        if (socket.connected) {
            const message = messages[messageIndex].split(' ');
            const word = message[wordIndex];
            socket.emit('message', word);
            wordIndex += 1;

            if (wordIndex >= message.length) {
                wordIndex = 0;
                messageIndex = (messageIndex + 1) % messages.length;
            }
        }
    };

    const interval = setInterval(sendData, 200);

    socket.on('disconnect', () => {
        console.log('Client disconnected');
        clearInterval(interval);
    });
});

app.get('/', (req, res) => {
    res.send('Socket.io server is running');
});

server.listen(8080, () => {
    console.log('Server is listening on http://localhost:8080');
});
